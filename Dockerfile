FROM openjdk:17-alpine
Volume /tmp
ADD /target/*.jar demo1-0.0.1-SNAPSHOT.jar
EXPOSE 8081
ENTRYPOINT ["java","-jar","/demo1-0.0.1-SNAPSHOT.jar"]
