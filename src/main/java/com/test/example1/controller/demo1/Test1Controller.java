package com.test.example1.controller.demo1;

import com.test.example1.controller.demo1.dto.ResponseObject;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class Test1Controller {
    @GetMapping("asci/{incoming}")
    public ResponseObject reverseString (@PathVariable String incoming) {
        ResponseObject responseObject = new ResponseObject();
        responseObject.setIncoming(incoming);

        StringBuilder sb = new StringBuilder();
        for (char c : incoming.toCharArray()) {
            sb.append((int) c);
        }
        responseObject.setOutGoing(StringUtils.hasLength(incoming) ?
                sb.toString() : "No Asci");
        return responseObject;
    }

    @GetMapping
    public ResponseObject reverseEmpty () {
        ResponseObject responseObject = new ResponseObject();
        responseObject.setIncoming("7687as8d7as76d");
        responseObject.setOutGoing("No Asci");
        return responseObject;
    }

    @GetMapping("asci")
    public ResponseObject reverseNull () {
        ResponseObject responseObject = new ResponseObject();
        responseObject.setIncoming("Empty Request");
        responseObject.setOutGoing("Empty Asci");
        return responseObject;
    }

    

}
