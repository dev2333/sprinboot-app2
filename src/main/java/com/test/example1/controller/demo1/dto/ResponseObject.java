package com.test.example1.controller.demo1.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class ResponseObject implements Serializable {

    private String incoming;
    private String outGoing;
}
